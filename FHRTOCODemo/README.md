
# Janitri Doc - LMTPDcoder static library 

## Introduction

LMTPDecoder static Library is used to monitor fetal heart rate, toco values, battVaues etc. 


## Getting Started

Bluetooth fetal heart docking call package The simulator supports the CPU architecture i386*86_64; the real machine supports the CPU architecture armv7 arm64:


## Installation

- Add LMDPDecoder SDK in {your iOS} project 

![Xcode Screenshot1](https://i.ibb.co/vvJqFbn/Screenshot-2023-02-04-at-11-03-52-AM.png)

![Xcode Screenshot2](https://i.ibb.co/kM1FMYS/Screenshot-2023-02-04-at-12-15-49-PM.png)

- Add these lame & LMTPDecoder.a files to ``` Link Binary with Libraries ``` 

![Xcode Screenshot2](https://i.ibb.co/xzmYc0X/Screenshot-2023-02-04-at-12-19-38-PM.png)

## Steps to follow. 

- ``` import LMTPDecoder.h ``` in any controller

- Similar for ``` import LKCDecodeHeart.h ```

- ```@import CoreBluetooth; ```

- Use ``` CBCentralManagerDelegate, CBPeripheralDelegate ``` methods for connecting with the BLE device. 

- once the device is connected. use further steps. 

- create an instance of LMTPDecoder. 

    ```self.decoder = [LMTPDecoder shareInstance];```

- use below methods for start & stop monitoring. 

![Xcode Screenshot2](https://i.ibb.co/nM9QmBK/Screenshot-2023-02-04-at-12-33-48-PM.png)




## Demo

Link to a demo video.

https://imgur.com/a/tEsWmkq
## Features

- fetal heart rate monitoring
- Toco monitoring
- batt & Signal monitoring
- fm & afm etc. 


## Screenshots

![App Screenshot](https://i.ibb.co/smMkwWM/IMG-3356.png)
![App Screenshot](https://i.ibb.co/F0CMvKD/IMG-3357.png)
![App Screenshot](https://i.ibb.co/tBrbsLN/IMG-3358.png)
