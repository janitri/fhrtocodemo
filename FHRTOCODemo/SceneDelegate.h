//
//  SceneDelegate.h
//  FHRTOCODemo
//
//  Created by Prashant.Dwivedi on 31/01/23.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

