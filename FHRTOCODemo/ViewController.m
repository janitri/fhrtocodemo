//
//  ViewController.m
//  FHRTOCODemo
//
//  Created by Prashant.Dwivedi on 31/01/23.
//

#import "ViewController.h"
#import "LMPD/LMTPDecoder.h"
#import "LMPD/LKCDecodeHeart.h"


#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height


@import CoreBluetooth;

@interface ViewController ()<UITableViewDataSource,UITableViewDelegate, CBCentralManagerDelegate, CBPeripheralDelegate>
@property (nonatomic, strong) CBCentralManager *cbManager;
@property (nonatomic, strong) CBPeripheral * testPeripheral;
@property (nonatomic, strong) CBCharacteristic * writeCharacteristic;
@property (nonatomic, strong) NSMutableArray *deviceArray;
@property (nonatomic, strong) NSMutableArray *readingsArray;
@property (nonatomic, strong) UITableView * deviceTable;
@property (nonatomic, strong) UITableView * readingsTable;
@property (strong, nonatomic) dispatch_queue_t bluetoothQueueLKC;

@property (nonatomic , copy)  NSMutableArray * heartRateArrayLKC;

@property (nonatomic, strong) UILabel * fmFlagLabel;
@property (assign, nonatomic) NSInteger afmMoveCount; // Automatic Kicks
@property (assign, nonatomic) NSInteger fmMoveCount; // Manual fetal movement

@property (strong , nonatomic) LMTPDecoder * decoder;
@property (nonatomic, assign) BOOL isConnected;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.decoder = [LMTPDecoder shareInstance];
    self.bluetoothQueueLKC = dispatch_queue_create("com.FHRTOCO.queue", NULL);
    
    CGFloat width = [[UIScreen mainScreen] bounds].size.width;
    CGFloat height = [[UIScreen mainScreen] bounds].size.height;
    
    UIButton * scanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    scanBtn.backgroundColor = [UIColor orangeColor];
    [scanBtn addTarget: self action:@selector(scanPeripheral) forControlEvents:UIControlEventTouchUpInside];
    [scanBtn setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
    [scanBtn setTitle:@"search device" forState:UIControlStateNormal];
    scanBtn.frame = CGRectMake(0, height - 64, width, 64);
    [self.view addSubview:scanBtn];
    
    UIButton * stopMontiorBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    stopMontiorBtn.backgroundColor = [UIColor orangeColor];
    [stopMontiorBtn addTarget: self action:@selector(stopMontiorFHRandSound) forControlEvents:UIControlEventTouchUpInside];
    [stopMontiorBtn setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
    [stopMontiorBtn setTitle:@"Stop Monitoring" forState:UIControlStateNormal];
    stopMontiorBtn.frame = CGRectMake(0,40, width, 44);
    [self.view addSubview:stopMontiorBtn];
    
    
    self.readingsArray = [[NSMutableArray alloc]init];
    self.readingsTable = [[UITableView alloc] initWithFrame:CGRectMake(0,84, width, (SCREEN_HEIGHT/2)-60)];
    self.readingsTable.backgroundColor = [UIColor systemGrayColor];
    self.readingsTable.delegate = self;
    self.readingsTable.dataSource = self;
    [self.view addSubview:self.self.readingsTable];
    
    self.deviceArray = [[NSMutableArray alloc] init];
    _deviceTable = [[UITableView alloc] initWithFrame:CGRectMake(0,SCREEN_HEIGHT/2, width, (SCREEN_HEIGHT/2)-60)];
    _deviceTable.backgroundColor = [UIColor lightGrayColor];
    _deviceTable.delegate = self;
    _deviceTable.dataSource = self;
    [self.view addSubview:self.deviceTable];
    
    
}


- (void)scanPeripheral{
    _cbManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
}

#pragma mark CBCentralManagerDelegate
// Called when the Bluetooth status of the mobile phone changes, or when the program is just executed
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    switch (central.state)
    {
            
        case CBCentralManagerStatePoweredOn:
        {
            NSDictionary *options =[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:CBCentralManagerScanOptionAllowDuplicatesKey];
            [self.cbManager scanForPeripheralsWithServices:nil options:options];//搜到设备后会有回调哦
        }
            break;
            
        case CBCentralManagerStatePoweredOff:
        {
            self.isConnected = NO;
            [self.deviceArray removeAllObjects];
            
        }
            break;
            
        default:
            NSLog(@"Central Manager did change state");
            break;
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    
    BOOL exists = NO;
    for (CBPeripheral *peripheralInArray in self.deviceArray) {
        if ([peripheralInArray.identifier isEqual:peripheral.identifier]) {
            exists = YES;
            break;
        }
    }
    
    if (!exists)
    {
        if(peripheral.name != nil)
        {
            [self.deviceArray addObject:peripheral];
            [self.deviceTable reloadData];
        }
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    
    [peripheral discoverServices:nil];
    [self.decoder startRealTimeAudioPlyer];
    
}

- (void)stopMontiorFHRandSound{
    NSLog(@"stop monitoring");
    [self.decoder stopMoniter]; // End Monitoring
    NSLog(@"readingsArray count =====>%lu",self.readingsArray.count);
    [self.readingsTable reloadData];
}

- (NSString *)getDeviceNameAtIndex:(NSInteger)index
{
    CBPeripheral *peripheral = [self.deviceArray objectAtIndex:index];
    return peripheral.name;
    
}

- (void)connectDeviceAtIndex:(NSInteger)index
{
    [self.cbManager stopScan];
    
    CBPeripheral *peripheral = [self.deviceArray objectAtIndex:index];
    NSLog(@"----> connect to device [%@]", peripheral);
    self.testPeripheral = peripheral;
    [self.cbManager connectPeripheral:self.testPeripheral options:nil];
    self.testPeripheral.delegate = self;
}

#pragma mark --CBPeripheralDelegate
// Search bluetooth service, click on a bluetooth device, start searching for his information
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    
    for (CBService *service in peripheral.services)
    {
        NSString *ustring = [self CBUUIDToString:service.UUID];
        [peripheral discoverCharacteristics:nil forService:service];
    }
}
#pragma mark --BLEUtility
- (NSString *) CBUUIDToString:(CBUUID *)inUUID
{
    unsigned char i[16];
    [inUUID.data getBytes:i];
    if (inUUID.data.length == 2) {
        return [NSString stringWithFormat:@"%02hhx%02hhx",i[0],i[1]];
    }
    else {
        uint32_t g1 = ((i[0] << 24) | (i[1] << 16) | (i[2] << 8) | i[3]);
        uint16_t g2 = ((i[4] << 8) | (i[5]));
        uint16_t g3 = ((i[6] << 8) | (i[7]));
        uint16_t g4 = ((i[8] << 8) | (i[9]));
        uint16_t g5 = ((i[10] << 8) | (i[11]));
        uint32_t g6 = ((i[12] << 24) | (i[13] << 16) | (i[14] << 8) | i[15]);
        return [NSString stringWithFormat:@"%08x-%04hx-%04hx-%04hx-%04hx%08x",g1,g2,g3,g4,g5,g6];
    }
    return nil;
}

//Search for Bluetooth features, after selecting a certain Bluetooth UUID service represents the service
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    NSLog(@"discover bluetooth");
    for (CBCharacteristic *characteristic in service.characteristics)
    {
        //        NSLog(@"Service: %@ :::::characteristic:%@", service.UUID, characteristic.UUID);
        
        if ([[self CBUUIDToString:characteristic.UUID] isEqualToString:@"fff1"])
        {
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
        }
        
        if ([[self CBUUIDToString:characteristic.UUID] isEqualToString:@"fff2"])
            //if ([characteristic.UUID isEqual:_writeUUID])
        {
            NSLog(@"Discovered write Characteristic");
            _writeCharacteristic = characteristic;
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (!characteristic) {
        return;
    }
    
    NSData *data = characteristic.value;
    
    LKCDecodeHeart * heart =  [self.decoder startDecoderWithCharacterData:data];
    if(heart){
        
        if (heart.afmFlag == 4) { // =4 // Indicates that the automatic fetal movement mark has been detected
            _afmMoveCount ++;
        }
        

        
        if(heart.signal >= 1 && heart.battValue >= 2 && heart.tocoValue >= 10){
            [self.readingsArray addObject: heart];
//            [self.readingsTable reloadData];
        }
        
        
    }
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if([tableView isEqual:self.readingsTable]){
        return self.readingsArray.count;
    }else if ([tableView isEqual:self.deviceTable]){
        return self.deviceArray.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([tableView isEqual:self.readingsTable]){
        return 104;
    }else if([tableView isEqual:self.deviceTable]) {
        return 44;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *registerID = @"janitriRegisterID";
    static NSString *readingsID = @"janitriReadingsID";
//    UITableViewCell *cell = [UITableViewCell class];
    
    if([tableView isEqual:self.deviceTable]){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:registerID];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:registerID];
        }
        NSString *deviceName = [self getDeviceNameAtIndex:indexPath.row];
        cell.textLabel.text = deviceName;
        cell.textLabel.textColor = [UIColor blackColor];
        cell.backgroundColor = [UIColor whiteColor];
        
        return cell;
    }else{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:readingsID];
        
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:registerID];
        }
        LKCDecodeHeart *heart = [self.readingsArray objectAtIndex:indexPath.row];
        
        NSMutableString *rowData = [[NSMutableString alloc]init];
        [rowData appendFormat:@"Heart rate: %ld \nHeart tocoValue: %ld\nHeart battValue: %ld\nHeart signal:%ld", heart.rate,heart.tocoValue,heart.battValue,heart.signal];
        cell.textLabel.text = rowData;
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.numberOfLines = 0;
        cell.backgroundColor = [UIColor whiteColor];
        
        return cell;
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([tableView isEqual:self.deviceTable]){
        [self connectDeviceAtIndex:indexPath.row];
    }
}



@end
